This repo is the static source code for [seerinteractive.bitbucket.io](https://seerinteractive.bitbucket.io/?from=sibb-readme) (hosted via [BitBucket Cloud](https://support.atlassian.com/bitbucket-cloud/docs/publishing-a-website-on-bitbucket-cloud/)).

[`/applets/*` - self-contained projects](https://seerinteractive.bitbucket.io/applets/?from=sibb-readme)
