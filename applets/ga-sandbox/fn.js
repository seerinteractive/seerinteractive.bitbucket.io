/**
 * Created by stephen on 11/10/15.
 */
window.out = {

	group          : function( msg ){
		//console.groupEnd();
		console.group( msg );
	},
	groupCollapsed : function( msg ){
		//console.groupEnd();
		console.groupCollapsed( msg );
	},
	groupEnd       : function( msg ){
		console.groupEnd();
		return true;
	},


	debug : function( msg ){
		console.debug( msg );
	},
	log  : function( msg ){
		console.log( msg );
	},
	info : function( msg ){
		console.info( msg );
	},
	warn : function( msg ){
		console.warn( msg );
	},

	error   : function( msg ){
		console.error( msg );
	},
	success : function( msg ){
		console.info( '%c' + msg, 'color:green;background:#DAFFDA;display:block' );
	}
};


function getFormDataAsObject( form, includeEmpty ){ // empty radios will be excluded regardless of arg:includeEmpty
	 var paramObj = {}, radiosCheckboxesSelects = {}, k, v;
	form = $(form);
	 $.each( getFormInputs(form), function(_, el) { // serializeArray ignores disabled inputs
		k = el.name, v = el.value;
		 if( el.type == 'radio' ){
			 radiosCheckboxesSelects[k] = 1;
			 if( el.checked ){
				 if( paramObj.hasOwnProperty( k ) ){
					 paramObj[k] = $.makeArray( paramObj[k] );
					 paramObj[k].push( v );
				 }
				 else {
					 paramObj[k] = v;
				 }
			 }
		 }else{
			 if( includeEmpty || v ){
				 if( paramObj.hasOwnProperty( k ) ){
					 paramObj[k] = $.makeArray( paramObj[k] );
					 paramObj[k].push( v );
				 }
				 else {
					 paramObj[k] = v;
				 }
			 }
		 }
});
	
	 if( includeEmpty ){
		 for( k in radiosCheckboxesSelects){
			 if( radiosCheckboxesSelects.hasOwnProperty( k ) && !paramObj.hasOwnProperty( k ) ){
				 paramObj[k] = '';
			 }
		 }
	 }
	 return paramObj;
}
function getFormDataAsQueryString( form, includeEmpty ){// empty radios will be excluded regardless of arg:includeEmpty
	return $.param( getFormDataAsObject( form, includeEmpty ) );
	//return form.serialize();
}



function getFormInputs( form ){
	return $(form).find( 'input[name]' );
}

function populateForm( form, data ){
	populateInputs( getFormInputs( form ), data );
}

function populateInputs( $inputs, data ){
	$inputs.each( function() {
		var $el = $(this), k = $el.attr('name'), v = '';
	   if (data.hasOwnProperty(k)) 
	    v = data[k];
		if( $el.is(':radio,:checkbox') ){
			if( v == $el.val() ){
				$el.attr('checked','');
			}else{
				$el.removeAttr('checked');
			}
		}else{
			$el.val( v );
		}
	});
}
function setFormValue( form, k, v ){
	var $input = $(form).find('input[name="'+k+'"]' );
	$input.addClass( 'value-updated' );
	$input.val( v ).trigger('change');
	setTimeout( function(){ 
		$input.removeClass( 'value-updated' );
	}, 5000 );
}

function timeText( seconds ){
	var num, unit, negative = ( seconds < 0 );
	seconds = Math.abs( seconds );
	num = seconds;
	unit = 'second';
	if( num >= 60 ){ num = num/60;   unit = 'minute';
	if( num >= 60 ){ num = num/60;   unit = 'hour';
	if( num >= 24 ){ num = num/24;   unit = 'day';
	if( num >= 7  ){ num = num/7;    unit = 'week';
	if( num >= 9  ){ num = num/4.35; unit = 'month';
	if( num >= 12 ){ num = num/12;   unit = 'year';
	}}}}}}
	if( num != 1 ) unit += 's';
	return num+' '+unit;
}


 
//TODO: urlParamsToObject( $.param( { 'a' : [1,2], 'b':{'One':1,'Two':2 } } ) ) // urlParamsToObject('a[]=1&a[]=2&b[One]=1&b[Two]=2')
function urlParamsToObject( str ){
	var r;
	if( !str ) return {};
	try {
		r = JSON.parse( '{"' + decodeURIComponent( str.replace( /^\?/, '' ).replace( /&/g, "\",\"" ).replace( /=/g, "\":\"" ) ) + '"}' )
	} catch( ex ){
		console.error( 'Failed to parse url params "' + str + '".' );
		return {}
	}
	for( var i in r )
		if( r[i] )
			r[i] = decodeURIComponent( r[i] ).replace( /\+/g, ' ' );
	
	console.warn( r );

	return r;

}


function getUrlParts( url ){
	var a, r = {};
	if( url && url.href )
		return url;//url = url.href;
	if( ! url )
		return window.location;
	a = document.createElement('a');
	a.href = url;
	
	r.protocol = a.protocol;
	r.host =     a.host;    
	r.hostname = a.hostname;
	r.port =     a.port;    
	r.pathname = a.pathname;
	r.hash =     a.hash;    
	r.search =   a.search;
	
	return r;
}

function updateBrowserUrlQuery( queryDataNew, extend ){
	var newURL = updateUrlQuery( null, queryDataNew, extend );
	window.history.replaceState( {}, document.title, newURL );
}
function updateBrowserUrlQueryFromForm( form, extend ){
	var newURL = updateUrlFromForm( null, form, extend );
	window.history.replaceState( {}, document.title, newURL );
}


function updateUrlQuery( url, queryDataNew, extend ){
	var 
	   url = getUrlParts( url ),
		qs = $.param( extend ? $.extend( urlParamsToObject( url.search ), queryDataNew ) : queryDataNew ),
		newURL = url.protocol
	            + "//" + url.host
	            + "" + url.pathname
	            + ( qs ? "?" + qs : "" );
	return newURL;
}


function updateUrlFromForm( url, form, prefix ){
	form = $(form);
	var data = getFormDataAsObject( form ) ;
	if( prefix ){
		if( typeof prefix != 'string' ){
			prefix = form.attr('id');
			if( ! prefix ) throw new Error( 'Cannot determine prefix for form.' );
		}
	}
	return updateUrlQuery( url, data );
}

