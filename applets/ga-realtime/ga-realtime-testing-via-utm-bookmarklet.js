/**
 * Created by stephen on 9/3/15.
 */
(window.w=function( v, sourceSuffix ){
	var 
		l=location,
		p = [ 'source','medium','campaign','term','content'],
	v = encodeURIComponent( v );
	l.href = 
	 l.origin
	 +l.pathname
	 +( 
	   (
	    l.search
	     /* remove existing utm params */
	     .replace(new RegExp('(^(\\?)|&)utm_(' +  p.join('|') + ')(=[^&]*)?', 'g' ), '$2' )
		  /* append utm params at end of querystring, before fragment */
	     +'&'+p.map(function(k,i){
	     		return 'utm_'+k+'='+v+(i||!sourceSuffix?"":
								 '_'+sourceSuffix+
								 '_'+(new Date()).toISOString().substring(0,16))// minute granularity ISO timestamp
	     	}).join('&')
	   ).replace( /^\??&+/, '?' )
	)
	+l.hash
})( 
	
	/* value - test value for all utm_* URL params */
	'SEER',

	/* source suffix - If non-empty, utm_source="{value}_{suffix}_{timestamp}" */
	'zztest'
)


